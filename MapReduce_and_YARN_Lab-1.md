![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# **MapReduce and YARN**

## **Lab-1 Exercise**

### **Objective**

To run the wordcount program using MapReduce.

### **Pre-requisite**

**1.** Java 1.7

**2.** Hadoop 2.X

### **Prepare:**

**1.** Locate the MapReduce example.jar file in &quot;Hadoop&quot; Folder. Common location will be

**C:\HADOOP\share\hadoop\mapreduce\hadoop-mapreduce-examples-2.4.1**

**Note: The path will change according to the user saving the file.**

**2.** Kindly find the .csv file to use in the lab exercise [books.csv](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/blob/master/Images/books.csv). Kindly place the lab in C:\ Drive or any other drive and keep a note of the path.

## **Let&#39;s run the commands now:**

**Step 1** - Open cmd in Administrative mode and move to &quot;C:/HADOOP/sbin&quot; and start cluster by using command.

**Start-all.cmd.**

![cmd](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/1.png)

![sbin](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/2.png)

![start-all](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/3.png)

**Step 2** - Move again to C:\ and if needed can run the rest of the script in non-safe mode otherwise can move to next step after moving back to C:\.

**hadoop dfsadmin -safemode leave**

![C drive](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/4.png)

![Safemode](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/5.png)

**Step 3** - Create an empty input directory/folder in HDFS using below given command.

**hadoop fs -mkdir /input\_dir**

![mkdir](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/6.png)

**Step 4** - Copy the input .csv file named books.csv in the input directory(input\_dir) of HDFS.

**hadoop fs -put <<"path of the file with extension">> /input\_dir**

![file](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/7.png)

**Step 5** - Verify that the input file is now available in HDFS directory(input\_dir).

**hadoop fs -ls /input\_dir**

![input](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/8.png)

**Step 6** – You can verify the input file via browser also. Go to below given link

**localhost:50070**

![browser](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/18.png)

**Step 7** – Here you can check the number of datanodes and namenodes also. Now to check the input file Go to **Utilities** -> **Browse the files system** -> Select the **input\_dir(the input file directory name).**

Inside that directory you can find the input file.

![Utilities](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/19.png)

![FileSystem](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/20.png)

![file](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/21.png)

**Step 8** – Now returning back to command line. Verify the content of the copied file.

**hadoop dfs -cat /input\_dir/<<"file\_name">>;**

![Verify](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/9.png)

**Step 9** - Run the Mapreduce.jar file and provide input and output directories.

**hadoop jar <<"jar file path with extension">> wordcount /input\_dir /output\_dir**

![jar](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/10.png)

![Exec](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/11.png)

**Step 10** - Verify content of the generated output file.

**hadoop dfs -cat /output\_dir/\***

![cat](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/12.png)

![output](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/13.png)

**Step 11** – You can verify the output file in browser.

**Localhost:50070**  ->  **Utilities**  ->  **Browse the file system**  ->  **output\_dir**  ->  **part-r-0000.**

This is the final output file which can even be downloaded.

![Directory](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/22.png)

![output](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/23.png)

**Step 12** - You can continue add any number of files and if you need to delete some files from the directory.

**hadoop fs -rm -r /input\_dir/<<"filename">>**

![remove](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/14.png)

**Step 13** – To delete the input and output directory.

**hadoop fs -rm -r /input\_dir**

**hadoop fs -rm -r /ouput\_dir**

![removedirectory](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/15.png)

**Step 14** – Stop all Hadoop resources before exit the command prompt.

**Stop-all.cmd**

![stop](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/16.png)

![exit](https://gitlab.com/Rorke540/mapreduce-and-yarn-lab/-/raw/master/Images/17.png)

**In the next lab we will run some more MapReduce Program.**

                                                              

                                                                        **This Ends the Exercise**

